/**
 * Module dependencies.
 */
var util = require('util')
  , OAuth2Strategy = require('passport-oauth').OAuth2Strategy
  , InternalOAuthError = require('passport-oauth').InternalOAuthError;


/**
 * `Strategy` constructor.
 *
 * The BDSwiss authentication strategy authenticates requests by delegating to
 * BDSwiss using the OAuth 2.0 protocol.
 *
 * Applications must supply a `verify` callback which accepts an `accessToken`,
 * `refreshToken` and service-specific `profile`, and then calls the `done`
 * callback supplying a `user`, which should be set to `false` if the
 * credentials are not valid.  If an exception occured, `err` should be set.
 *
 * Options:
 *   - `clientID`      your BDSwiss application's client id
 *   - `clientSecret`  your BDSwiss application's client secret
 *   - `callbackURL`   URL to which BDSwiss will redirect the user after granting authorization
 *
 * Examples:
 *
 *     passport.use(new BDSwissStrategy({
 *         clientID: '123-456-789',
 *         clientSecret: 'shhh-its-a-secret'
 *         callbackURL: 'https://www.example.net/auth/bdswiss/callback'
 *       },
 *       function(accessToken, refreshToken, profile, done) {
 *         User.findOrCreate(..., function (err, user) {
 *           done(err, user);
 *         });
 *       }
 *     ));
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options, verify) {
  options = options || {};
  options.authorizationURL = options.authorizationURL || 'http://vip.bdswiss.com/oauth/dialog/authorize';
  options.tokenURL = options.tokenURL || 'http://vip.bdswiss.com/oauth/token';
  
  OAuth2Strategy.call(this, options, verify);
  this.name = 'bdswiss';
}

/**
 * Inherit from `OAuth2Strategy`.
 */
util.inherits(Strategy, OAuth2Strategy);


/**
 * Retrieve user profile from BDSwiss.
 *
 * This function constructs a normalized profile, with the following properties:
 *
 *   - `provider`         always set to `bdswiss`
 *   - `id`
 *   - `displayName`
 *   - `givenName`
 *   - `familyName`
 *   - `country`
 *   - `balance`
 *   - `pnl`
 *   - `email`
 *
 * @param {String} accessToken
 * @param {Function} done
 * @api protected
 */

Strategy.prototype.userProfile = function(accessToken, done) {
  this._oauth2.get('http://vip.bdswiss.com/api/userinfo', accessToken, function (err, body, res) {
    if (err) { return done(new InternalOAuthError('failed to fetch user profile', err)); }
    try {
      var json = JSON.parse(body);
      var profile = { provider: 'bdswiss' };
      profile.id = json.id;
      profile.displayName = json.displayName;
      profile.givenName = json.givenName || '';
      profile.familyName = json.familyName || '';
      profile.country = json.country;
      profile.balance = json.balance;
      profile.pnl = json.pnl;
      profile.email = json.email;
      profile.currency = json.currency;
      profile._raw = body;
      profile._json = json;
      done(null, profile);
    } catch(e) {
      done(e);
    }
  });
}


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
