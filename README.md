# passport-bdswiss

[Passport](https://github.com/jaredhanson/passport) strategy for authenticating
with [BDSwiss](http://bdswiss.com/) using the OAuth 2.0 API.

This module lets you authenticate using BDSwiss in your Node.js applications.
By plugging into Passport, BDSwiss authentication can be easily and
unobtrusively integrated into any application or framework that supports
[Connect](http://www.senchalabs.org/connect/)-style middleware, including
[Express](http://expressjs.com/).

## Install

    $ npm install passport-bdswiss

## Usage

#### Configure Strategy

The BDSwiss authentication strategy authenticates users using an BDSwiss
account and OAuth 2.0 tokens.  The strategy requires a `verify` callback, which
accepts these credentials and calls `done` providing a user, as well as
`options` specifying a client ID, client secret, and callback URL.

    passport.use(new BDSwissStrategy({
        clientID: BDSWISS_CLIENT_ID,
        clientSecret: BDSWISS_CLIENT_SECRET,
        callbackURL: "http://127.0.0.1:3000/auth/bdswiss/callback"
      },
      function(accessToken, refreshToken, profile, done) {
        User.findOrCreate({ bdswissId: profile.id }, function (err, user) {
          return done(err, user);
        });
      }
    ));

#### Authenticate Requests

Use `passport.authenticate()`, specifying the `'bdswiss'` strategy, to
authenticate requests.

For example, as route middleware in an [Express](http://expressjs.com/)
application:

    app.get('/auth/bdswiss',
      passport.authenticate('bdswiss'));

    app.get('/auth/bdswiss/callback', 
      passport.authenticate('bdswiss', { failureRedirect: '/login' }),
      function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
      });

## Credits

  - [Paulius Uza](http://github.com/pauliusuza)

## License

[The MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 2011-2013 Paulius Uza <[http://inruntime.com/](http://inruntime.com/)>
